package com.everis.myapplication.list.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.everis.myapplication.R
import com.everis.myapplication.list.ListModel
import kotlinx.android.synthetic.main.fragment_list_row.view.*

class ListAdapter(private val lData: List<ListModel>) :
    RecyclerView.Adapter<ListAdapter.ListViewHoler>() {

    var itemPressed: ((ListModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHoler {
        return ListViewHoler(
            LayoutInflater.from(parent.context).inflate(
                R.layout.fragment_list_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = lData.size

    override fun onBindViewHolder(holder: ListViewHoler, position: Int) {
        holder.bind(lData[position])
    }

    inner class ListViewHoler(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(data: ListModel) {
            itemView.run {
                tvListRowTitle?.text = data.title
                tvListRowDescription?.text = data.description
                setOnClickListener { itemPressed?.invoke(data) }
            }
        }
    }
}
package com.everis.myapplication.global

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.everis.myapplication.R
import com.everis.myapplication.global.browser.GlobalBrowserFragment
import com.everis.myapplication.global.list.GlobalListFragment
import com.everis.myapplication.global.main.GlobalMainFragment
import com.everis.myapplication.global.profile.GlobalProfileFragment
import com.everis.myapplication.global.tutorial.GlobalTutorialFragment
import kotlinx.android.synthetic.main.activity_global.*

class GlobalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_global)

        if (savedInstanceState == null) {
            bnvGlobal?.selectedItemId = R.id.action_main
            showMainView()
        }

        bnvGlobal?.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_list -> {
                    showListView()

                    true
                }

                R.id.action_tutorial -> {
                    showTutorialView()

                    true
                }

                R.id.action_main -> {
                    showMainView()

                    true
                }

                R.id.action_browser -> {
                    showBrowserView()

                    true
                }
                R.id.action_profile -> {
                    showProfileView()

                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun showListView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flGlobalContainer, GlobalListFragment.newInstance())
            .commit()
    }

    private fun showTutorialView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flGlobalContainer, GlobalTutorialFragment.newInstance())
            .commit()
    }

    private fun showMainView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flGlobalContainer, GlobalMainFragment.newInstance())
            .commit()
    }

    private fun showBrowserView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flGlobalContainer, GlobalBrowserFragment.newInstance())
            .commit()
    }

    private fun showProfileView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flGlobalContainer, GlobalProfileFragment.newInstance())
            .commit()
    }

    companion object {
        fun newInstance(context: Context) = Intent(context, GlobalActivity::class.java)
    }
}

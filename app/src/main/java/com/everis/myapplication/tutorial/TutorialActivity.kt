package com.everis.myapplication.tutorial

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.everis.myapplication.R
import kotlinx.android.synthetic.main.activity_tutorial.*

class TutorialActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        vpTutorial?.adapter = TutorialAdapter(fragmentManager = supportFragmentManager)
        tlTutorial?.setupWithViewPager(vpTutorial)
    }

    companion object {
        fun newInstance(context: Context) = Intent(context, TutorialActivity::class.java)
    }
}

package com.everis.myapplication.tutorial

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.everis.myapplication.tutorial.first.FirstTutorialFragment
import com.everis.myapplication.tutorial.second.SecondTutorialFragment

class TutorialAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val views = listOf(
        FirstTutorialFragment.newInstance(),
        SecondTutorialFragment.newInstance()
    )

    private val titles = listOf(
        "First",
        "Second"
    )

    override fun getItem(position: Int): Fragment = views[position]

    override fun getCount(): Int = views.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]

}
package com.everis.myapplication.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.everis.myapplication.R
import com.everis.myapplication.global.GlobalActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        etLogin?.doAfterTextChanged {
            bLogin?.isEnabled = it?.isNotEmpty() ?: false
        }

        bLogin?.setOnClickListener {
            startActivity(GlobalActivity.newInstance(context = this))
        }
    }
}
